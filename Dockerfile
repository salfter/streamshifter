FROM php:fpm
# ffmpeg source build adapted from https://gist.github.com/rafaelbiriba/7f2d7c6f6c3d6ae2a5cb
# monitor https://git.ffmpeg.org/gitweb/ffmpeg.git/heads for new releases
# (currently on 4.4)
RUN apt-get update && apt-get -y install autoconf automake build-essential git-core libass-dev libgpac-dev libsdl1.2-dev libtheora-dev libtool libvdpau-dev libvorbis-dev libx11-dev libxext-dev libxfixes-dev pkg-config texi2html zlib1g-dev libmp3lame-dev nasm gcc yasm && true && mkdir ~/ffmpeg_sources && cd ~/ffmpeg_sources && curl -L https://github.com/mstorsjo/fdk-aac/archive/refs/tags/v2.0.2.tar.gz | tar xzf - && cd fdk-aac-2.0.2 && autoreconf -fiv && ./configure --prefix="$HOME/ffmpeg_build" --disable-shared && make -j16 && make install && make distclean && cd ~/ffmpeg_sources && curl -L https://sourceforge.net/projects/lame/files/lame/3.99/lame-3.99.5.tar.gz | tar xzf - && cd lame-3.99.5 && ./configure --prefix="$HOME/ffmpeg_build" --enable-nasm --disable-shared && make -j16 && make install && make distclean && cd ~/ffmpeg_sources && git clone --depth 1 --branch release/4.4 https://git.ffmpeg.org/ffmpeg.git ffmpeg && cd ffmpeg && PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig" && export PKG_CONFIG_PATH && ./configure --prefix="$HOME/ffmpeg_build" --extra-cflags="-I$HOME/ffmpeg_build/include" --extra-ldflags="-L$HOME/ffmpeg_build/lib" --bindir="$HOME/bin" --extra-libs="-ldl" --enable-gpl --enable-libass --enable-libfdk-aac --enable-libmp3lame --enable-nonfree && make -j16 && make install && cp ffmpeg /usr/bin/ && make distclean && apt-get -y remove autoconf automake build-essential git-core libtool pkg-config texi2html zlib1g-dev nasm gcc yasm && apt-get -y autoremove && rm -rf ~/ffmpeg_sources /var/lib/apt/lists/*
COPY record.sh cleanup.sh /usr/bin/
COPY *.php /var/www/html/
ENV BASEURL "$BASEURL"
ENV CHANNELS "${CHANNELS:-2}"
ENV BITRATE "${BITRATE:-32}"
RUN (cd /var/www/html && curl -L https://github.com/JamesHeinrich/getID3/archive/refs/tags/v1.9.21.tar.gz | tar xzf - && mv getID3-* getid3)
RUN (cd /var/www/html && curl -L https://github.com/mediaelement/mediaelement/archive/refs/tags/5.0.2.tar.gz | tar xzf - && mv mediaelement-* mediaelement)
RUN (cd /var/www/html && curl -L https://github.com/mediaelement/mediaelement-plugins/archive/refs/tags/2.6.1.tar.gz | tar xzf - && mv mediaelement-plugins-* mediaelement-plugins)
VOLUME /var/www/html/
