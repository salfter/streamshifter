StreamShifter
=============

Capture streaming audio from your favorite sources to build a custom podcast stream.  I used this for a number of years on a bog-standard Linux/Nginx/PHP-FPM server, and have recently containerized it for use with Docker.  It'll record live audio captured from a URL, transcoding it into a common format along the way.  You can listen to the most recent stream as it records with HTTP live streaming, or you can listen later to the podcast.

StreamShifter actually uses MediaElement.js now...it wasn't previously.  Buttons to skip forward and back 30 seconds have been added to the player, which are convenient for skipping past commercials.

The Dockerfile now builds ffmpeg from source, so it can include HE-AAC support.

Standalone Instructions
-----------------------

Make sure you've pulled in the getid3 and mediaelement dependencies...easiest way is to do that in the intiaial checkout:

```
git --recursive https://gitlab.com/salfter/streamshifter
```

You'll need a webserver with PHP support, as well as curl and ffmpeg, which are used by the recording script.  Clone into a directory that your webserver will handle, and make sure the server is set to run index.php as an index file.

record.sh gets called (most likely as a cronjob) to capture and save audio.  It needs to be called with the CHANNELS and BITRATE environment variables defined as described below.  An example:

```
CHANNELS=2 BITRATE=32 record.sh 10 test "https://14983.live.streamtheworld.com/KDWNAMAAC.aac?tdsdk=js-2.9&pname=TDSdk&pversion=2.9&banners=320x50&sbmid=ddc0cb91-c9a7-46c4-bf53-85841c22c8e4" Test
```

cleanup.sh should be set up to run daily.

rss.php depends on the BASEURL environment variable:

```
$baseurl=getenv("BASEURL");
```

I don't know where offhand you'd set environment variables for a PHP script that's run by a webserver, so you might find it easier to hardcode your server's address here:

```
$baseurl="https://streams.alfter.us/";
```

Docker Instructions
-------------------

This container builds on the php:fpm container to include tools and scripts for capturing streaming audio, transcoding it to HE-AAC, and storing it as a set of HTTP live streams and a podcast.

Three environment variables are exposed to configure StreamShifter:

* BASEURL: base URL under which the site will be served (required)
* CHANNELS: number of audio channels to encode (default 2)
* BITRATE: encoding bitrate, kbps (default: 32)

The recordings and scripts to be served are stored in a named volume.  Configure your webserver to serve it under a vhost by whatever means you would configure it to serve php:fpm.  

Start the PHP FPM server with something like this:

```
docker run -d --name streamshifter -e BASEURL=https://streamshifter.alfter.us/ -v streamshifter-data:/var/www/html salfter/streamshifter
```

It runs on port 9000.  You should set your webserver container to hand off PHP requests to it.

To schedule recordings, use the record.sh script in a running container (use ```docker exec```) with four parameters, all required:

* duration, seconds
* short name for the stream (no spaces, used as part of filenames)
* stream URL
* long name for the stream (may have spaces, written into stream metadata)

Use whatever task scheduler your system provides (crontab, systemd timer unit, etc.) to run the script at the appropriate time.  This example saves 10 seconds of the stream from KDWN, a talk-radio station in Las Vegas (streaming URL subject to change at their whim):

```
docker exec -it streamshifter record.sh 10 test "https://14983.live.streamtheworld.com/KDWNAMAAC.aac?tdsdk=js-2.9&pname=TDSdk&pversion=2.9&banners=320x50&sbmid=ddc0cb91-c9a7-46c4-bf53-85841c22c8e4" Test
```

There is also a cleanup script you can run periodically that will remove everything more than one week old:

```
docker exec -it streamshifter cleanup.sh
```
