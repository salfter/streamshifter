<?php
  require_once('./getid3/getid3/getid3.php');
  $getid3=new getID3;

  $baseurl=getenv("BASEURL");

  $doc = new DOMDocument("1.0", "UTF-8");
  $root = $doc->createElement("rss");
  $attr = $doc->createAttribute("version");

  $channel=$doc->createElement("channel");
  $channel->appendChild($doc->createElement("title","recordings"));
  $channel->appendChild($doc->createElement("link",$baseurl));
  $channel->appendChild($doc->createElement("description","captured audio streams"));
  $latest=0;

  $c=0; // sort entries into reverse chronological order
  if ($h=opendir("/var/www/html/audio"))
  {
    while (($e=readdir($h))!==false)
      if (strlen($e)>4)
        if (substr_compare($e,".m4a",-4,4)===0)
         $arr[$c++]=filemtime("/var/www/html/audio/".$e)." ".$e;
    closedir($h);
  }
  sort($arr, SORT_NUMERIC);
  $arr=array_reverse($arr);

  foreach ($arr as $e)
  {
    $e=substr($e,strpos($e," ")+1);
    $item=$doc->createElement("item");
    
    // extract title from metadata
    $fi=$getid3->analyze("/var/www/html/audio/".$e);
    getid3_lib::CopyTagsToComments($fi);
    $item->appendChild($doc->createElement("title",$fi["comments_html"]["title"][0]));
    $item->appendChild($doc->createElement("author",$fi["comments_html"]["artist"][0]));
    
    //$item->appendChild($doc->createElement("title",$e));

    $item->appendChild($doc->createElement("link",$baseurl."audio/".$e));
    $encl=$doc->createElement("enclosure");
    $enclURL=$doc->createAttribute("url");
    $enclURL->value=$baseurl."audio/".$e;
    $encl->appendChild($enclURL);
    $enclLength=$doc->createAttribute("length");
    $enclLength->value=filesize("/var/www/html/audio/".$e);
    $encl->appendChild($enclLength);
    $enclType=$doc->createAttribute("type");
    $enclType->value="audio/mp4a-latm";
    $encl->appendChild($enclType);
    $item->appendChild($encl);
    $item->appendChild($doc->createElement("pubDate",gmdate(DATE_RSS,date(filemtime("/var/www/html/audio/".$e)))));
    $channel->appendChild($item);
    if ($latest<filemtime("/var/www/html/audio/".$e))
      $latest=filemtime("/var/www/html/audio/".$e);
  }

  $channel->appendChild($doc->createElement("lastBuildDate",gmdate(DATE_RSS,date($latest))));
  $root->appendChild($channel);
  $attr->value = "2.0";
  $root->appendChild($attr);
  $doc->appendChild($root);

  header("Content-Type: text/xml");
  echo $doc->SaveXML();
?>
