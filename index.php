<!DOCTYPE html>
<html lang="en">
<head>
    <title>Live Audio and Podcasts</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="mediaelement/build/mediaelementplayer.css">
    <link rel="stylesheet" href="mediaelement-plugins/dist/jump-forward/jump-forward.css">
    <link rel="stylesheet" href="mediaelement-plugins/dist/skip-back/skip-back.css">
</head>
<body>
    <h1>Live Audio and Podcasts</h1>
    <ul>
    <?php
    $arr=[];
    $c=0;
    if ($h=opendir("audio"))
    {
        while (($e=readdir($h))!==false)
        {
        if (strlen($e)>4)
            if (substr_compare($e,".m4a",-4,4)===0)
            $arr[$c++]="<li><a href=\"audio/".$e."\">".$e."</a> (<a href=\"#\" onclick=\"Load('audio/".$e."', 'audio/m4a')\">Load in Player</a>)</li>\n";
        }
        closedir($h);
    }
    if ($h=opendir("."))
    {
        while (($e=readdir($h))!==false)
        {
        if (strlen($e)>5)
            if (substr_compare($e,".m3u8",-5,5)===0)
            $arr[$c++]="<li><a href=\"".$e."\">".$e."</a> (<a href=\"#\" onclick=\"Load('".$e."', 'application/x-mpegURL')\">Load in Player</a>)</li>\n";
        }
        closedir($h);
    }
    sort($arr);
    foreach ($arr as $item)
        echo $item;
    ?>
    <li><a href="rss.php">RSS Feed</a></li>
    </ul>
    <br />
    <div class="media-wrapper">
        <audio id="player1" width="400" height="45" controls preload="none"></audio>
    </div>

    <br /><span id="filename"></span>

    <script src="mediaelement/build/mediaelement-and-player.min.js"></script>
    <script src="mediaelement-plugins/dist/jump-forward/jump-forward.js"></script>
    <script src="mediaelement-plugins/dist/skip-back/skip-back.js"></script>
    <script>
	    var mediaElements = document.querySelectorAll('video, audio');

	    for (var i = 0, total = mediaElements.length; i < total; i++) {

		    var features = ['playpause', 'current', 'skipback', 'progress', 'jumpforward', 'duration'];

		    new MediaElementPlayer(mediaElements[i], {
			    autoRewind: false,
			    features: features,
		    });
	    }

        function Load(src, mime_type)
        {
            document.getElementById("player1").src=src;
            document.getElementById("player1").type=mime_type;
            document.getElementById("player1").load();
            document.getElementById("player1").play();
            document.getElementById("filename").innerHTML=src;
        }

    </script>
</body>
</html>
