#!/usr/bin/env bash
source /etc/profile
mkdir -p /var/www/html/audio
cd /var/www/html
find . -name $2-\*.ts -delete
find . -name $2\*.m3u8 -delete

#transopt="aac -ac ${CHANNELS} -ab ${BITRATE}k -ar 22050" # AAC-LC
transopt="libfdk_aac -profile:a aac_he_v2 -ac ${CHANNELS} -ab ${BITRATE}k -ar 22050" # HE-AAC v2

curl -sm $1 "$3" 2>/dev/null | ffmpeg -i - -acodec $transopt -metadata title="$4 - `date +'%d %b %Y'`" -metadata artist="$4" -f segment -segment_list $2.m3u8 -segment_time 20 $2-%04d.ts 2>/dev/null
ffmpeg -i $2.m3u8 -acodec copy -movflags +faststart -metadata title="$4 - `date +'%d %b %Y'`" -metadata artist="$4" audio/$2-`date +%Y%m%d`.m4a 2>/dev/null
